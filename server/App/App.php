<?php
/**
 * class App
 * start depends classes
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace App;

use StorchakProject\framework\src\Route;
use StorchakProject\framework\src\SessionUsers;

class App
{
    public function __construct()
    {
        require_once '../packages/storchakproject/framework/src/function.php';

        $session = new SessionUsers();
        $session->getSession('id');
        require_once ROOT . '/routes/web.php';
        Route::dispatch(URL);
    }
}
