<?php
/**
 * class UserModel
 * models for users
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Models;

use StorchakProject\framework\src\Models\Model;
use StorchakProject\framework\src\SessionUsers;

class UserModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'users';
    /**
     * @var string
     */
    protected string $table_join = 'users inner join gender on users.gender=gender.gender_id';

    /**
     * @var bool
     */
    protected bool $created_at = true;
    /**
     * @var bool
     */
    protected bool $updated_at = true;
    /**
     * @var bool
     */
    protected bool $closeSelection = false;

    /**
     * @param array $array
     * @return string[]
     */
    public static function create(array $array): array
    {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);
        $userModel = new self;
        $arr = [];
        $arr['first_name'] = $array['first_name'];
        $arr['last_name'] = $array['last_name'];
        if (($array['gender'] != 'Select gender status')) {
            $arr['gender'] = $array['gender'];
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/user/register');
        }
        $arr['email'] = $array['email'];
        $email = $array['email'];
        if (!empty($userModel->whereOne(['email'], 'email', '=', $email))) {
            return ['text' => 'This email exists. Log in'];
        }
        $arr['password'] = password_hash($array['password'], PASSWORD_BCRYPT);
        return $userModel->insert($arr);
    }

    /**
     * @param string $id
     * @return array
     */
    public static function load(string $id): array
    {
        $userModel = new self();
        $param = ['*'];
        $search = 'users.id';
        $operator = '=';
        $result = $userModel->whereOne($param, $search, $operator, $id);
        $lock = [$result['password'], $result['created_at'], $result['updated_at']];
        return array_filter($result, fn($elem) => (!in_array($elem, $lock)));
    }

    /**
     * @param array $array
     * @return array
     */
    public static function updates(array $array): array
    {
        $userModel = new self();
        $arr = [];
        $arr['updated_at'] = date('Y-m-d');
        if ($_SESSION['id'] == 'root') {
            $id = $array['id'];
        } else {
            $id = $_SESSION['id'];
        }
        if (!empty($array['password'])) {
            $arr['password'] = password_hash($array['password'], PASSWORD_BCRYPT);
        }
        if ($array['email_old'] != $array['email']) {
            $arr['email'] = $array['email'];
            $email = htmlspecialchars($array['email']);
            if (!empty($userModel->whereOne(['email'], 'email', '=', $email))) {
                return [];
            }
        }
        if (strlen($array['gender']) == 1) {
            $arr['gender'] = $array['gender'];
        }
        if (($array['img'] != 'http://localhost:8184/images/users/user_foto_default.png' && !empty($array['img']))) {
            $arr['img'] = $array['img'];
        }
        $arr['first_name'] = $array['first_name'];
        $arr['last_name'] = $array['last_name'];
        return $userModel->update($arr, 'id', $id);
    }

    /**
     * @param string $id
     * @return void
     */
    public static function del(string $id): void
    {
        $userModel = new UserModel();
        $userModel->delete('id', '=', $id);
        if ($_SESSION['id'] != $_ENV['ROOT_ID']) {
            $session = new SessionUsers();
            $session->delSession('id');
            unset($_SESSION['id']);
        }
    }

    /**
     * @param array $array
     * @return bool|void
     */
    public static function authorization(array $array): bool
    {
        $userModel = new self();
        $password = htmlspecialchars($array['password']);
        $email = htmlspecialchars($array['email']);
        $param = ['id', 'password'];
        $search = 'email';
        $operator = '=';
        $id = $email;
        $result = $userModel->whereOne($param, $search, $operator, $id);
        if (!empty($result)) {
            if (password_verify($password, $result['password'])) {
                $_SESSION['id'] = $result['id'];
                $session = new SessionUsers();
                $_SESSION['id']=$session->setSession('id', $result['id']);
                return true;
            } else {
                return false;
            }
        }
    }
}
