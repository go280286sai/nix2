<?php
/**
 * class MainModel
 * models main page
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Models;

use StorchakProject\framework\src\Models\Model;

class MainModel extends Model
{
    /**
     * @var string
     */
    protected string $table_join =
        'articles inner join users on users.id=articles.user_id inner join sorts on sorts.sort_id=articles.sort';

    /**
     * @return array
     */
    public static function getAll(): array
    {
        $mainModel = new self;
        return $mainModel->findAll();
    }
}
