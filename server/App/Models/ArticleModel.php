<?php
/**
 * class ArticleModel
 * models for articles
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Models;

use StorchakProject\framework\src\Models\Model;

class ArticleModel extends Model
{
    /**
     * @var string
     */
    protected string $table_join =
        'articles inner join users on users.id=articles.user_id inner join sorts on sorts.sort_id=articles.sort';

    protected string $table = 'articles';
    /**
     * @var bool
     */
    protected bool $created_at = true;
    /**
     * @var bool
     */
    protected bool $updated_at = true;

    /**
     * @param array $array
     * @return void
     */
    public static function create(array $array): void
    {
        $articleModel = new self;
        $arr = [];
        $arr['title'] = htmlentities($array['title'], ENT_QUOTES, 'utf-8');
        $arr['short'] = htmlentities($array['short'], ENT_QUOTES, 'utf-8');
        $arr['description'] = htmlentities($array['description'], ENT_QUOTES, 'utf-8');
        $arr['sort'] = $array['sort'];
        $arr['user_id'] = $array['id'];
        $articleModel->insert($arr);
    }

    /**
     * @param $session
     * @return void
     */
    public static function select($session): void
    {
        $articleModel = new self;
        $arrays = ['article_id', 'title', 'short', 'articles.created_at', 'articles.updated_at'];
        $where = $session;
        $mArticle = $articleModel->where($arrays, 'user_id', '=', $where);
        $array = [];
        $mas = [];
        foreach ($mArticle as $arr) {
            foreach ($arr as $index => $item) {
                $mas[$index] = $item;
            }
            $array[] = $mas;
        }
        echo json_encode($array);
    }

    /**
     * @param array $array
     * @return array
     */
    public static function load(array $array): array
    {
        $articleModel = new self();
        $param = ['article_id', 'title', 'short', 'description', 'name'];
        $search = 'article_id';
        $operator = '=';
        $id = $array['id'];
        $arr = $articleModel->whereOne($param, $search, $operator, $id);
        foreach ($arr as $item => $value) {
            if ($item == 'title') {
                $arr[$item] = html_entity_decode($value);
            }
            if ($item == 'short') {
                $arr[$item] = html_entity_decode($value);
            }
            if ($item == 'description') {
                $arr[$item] = html_entity_decode($value);
            }
        }
        return $arr;
    }

    /**
     * @param array $array
     * @return void
     */
    public static function updates(array $array): void
    {
        $arr = [];
        $arr['updated_at'] = date('Y-m-d');
        $arr['title'] = htmlentities($array['title'], ENT_QUOTES, 'utf-8');
        $arr['short'] = htmlentities($array['short'], ENT_QUOTES, 'utf-8');
        $arr['description'] = htmlentities($array['description'], ENT_QUOTES, 'utf-8');
        if (!empty($array['sort'])) {
            $arr['sort'] = $array['sort'];
        }
        $id = $array['id'];
        $articleModel = new self();
        $articleModel->update($arr, 'article_id', $id);
    }

    /**
     * @param $arr
     * @return void
     */
    public static function del($arr): void
    {
        $articleModel = new ArticleModel();
        $id = htmlspecialchars($arr['id']);
        $articleModel->delete('article_id', '=', $id);
    }

    /**
     * @param int $id
     * @return array
     */
    public static function getSort(int $id): array
    {
        $id = htmlspecialchars($id);
        $mainModel = new self;
        $result = $mainModel->where(['*'], 'sort', '=', $id);
        $array = [];
        foreach ($result as $item) {
            $lock = [
                $item['password'],
                $item['user_id'],
                $item['updated_at'],
                $item['img'],
                $item['name'],
                $item['gender'],
                $item['email']
            ];
            $mas = array_filter($item, fn($elem) => (!in_array($elem, $lock)));
            $array[] = $mas;
        }
        return $array;
    }

    /**
     * @param $id
     * @return array
     */
    public static function getArticle($id): array
    {
        $mainModel = new self;
        return $mainModel->where([
            'title',
            'description',
            'first_name',
            'last_name',
            'articles.created_at',
            'article_id'
        ], 'article_id', '=', $id);
    }
}
