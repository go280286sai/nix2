<?php
/**
 * class AdminModel
 * run methods
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Models;

use StorchakProject\framework\src\Models\Model;

class AdminModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'users';
    /**
     * @var string
     */
    protected string $table_join = 'users inner join gender on users.gender=gender.gender_id';
    /**
     * @var bool
     */
    protected bool $created_at = true;
    /**
     * @var bool
     */
    protected bool $updated_at = true;
    /**
     * @var bool
     */
    protected bool $closeSelection = false;

    /**
     * @param array $array
     * @return bool
     */
    public static function authorization(array $array): bool
    {
        new self();
        $password = htmlspecialchars($array['password']);
        $email = htmlspecialchars($array['email']);
        if ($password == $_ENV['ROOT_PASSWORD'] && $email == $_ENV['ROOT_EMAIL']) {
            $_SESSION['id'] = $_ENV['ROOT_ID'];
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param array $array
     * @return void
     */
    public static function select(array $array): void
    {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);
        $adminModel = new self;
        if (empty($array['search'])) {
            $mAdmin = $adminModel->findAll();
        } else {
            $search = htmlspecialchars($array['search']);
            $mAdmin = $adminModel->where([
                'id',
                'first_name',
                'last_name',
                'name',
                'email',
                'created_at',
                'updated_at'
            ], $search, '', '');
        }
        $array = [];
        $mas = [];
        foreach ($mAdmin as $arr) {
            foreach ($arr as $index => $item) {
                $mas[$index] = $item;
            }
            $array[] = $mas;
        }
        echo json_encode($array);
    }

    /**
     * @param string $id
     * @return array
     */
    public static function load(string $id): array
    {
        $userModel = new self();
        $param = ['*'];
        $search = 'users.id';
        $operator = '=';
        return $userModel->whereOne($param, $search, $operator, $id);
    }
}
