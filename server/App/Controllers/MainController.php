<?php
/**
 * class MainController
 * output in main page
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Controllers;

use App\Models\MainModel;

class MainController extends BaseController
{
    /**
     * @return bool
     */
    public function index(): bool
    {
        $data = MainModel::getAll();
        $volume = array_column($data, 'article_id');
        array_multisort($volume, SORT_DESC, $data);
        return $this->view(MAIN_INDEX, $data);
    }

    /**
     * @return bool
     */
    public function contact(): bool
    {
        return $this->view(MAIN_CONTACT, ['title' => 'Contact']);
    }
}
