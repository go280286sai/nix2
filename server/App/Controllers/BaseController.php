<?php
/**
 * class BaseController
 * connects to the page rendering class
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Controllers;

use StorchakProject\framework\src\Controllers\Controller;

class BaseController extends Controller
{

}
