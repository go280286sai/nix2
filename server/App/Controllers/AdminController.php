<?php
/**
 * class AdminController
 * methods for admin
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Controllers;

use App\Models\AdminModel;
use App\Models\UserModel;

class AdminController extends BaseController
{
    /**
     * @return bool
     */
    public function index(): bool
    {
        return $this->view(ADMIN_LOGIN, ['title' => 'Authorization for admin']);
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        return $this->view(ADMIN_LOGIN, ['title' => 'Authorization for admin']);
    }

    /**
     * @return bool|void
     */
    public function authorization()
    {
        if (!empty($_POST)) {
            if (AdminModel::authorization($_POST)) {
                header('Location: ' . $_ENV['APP_URL'] . ADMIN_UPDATE);
            } else {
                return $this->view(ADMIN_LOGIN, ['text' => 'Вы ввели не верный логин или пароль']);
            }
        } else {
            return $this->view(ADMIN_LOGIN, ['text' => 'Вы не ввели логин или пароль']);
        }
    }

    /**
     * @return void
     */
    public function find(): void
    {
        if (isset($_SESSION['id']) == $_ENV['ROOT_ID']) {
            AdminModel::select($_POST);
        }
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        if (isset($_SESSION['id']) == $_ENV['ROOT_ID'] && isset($_POST['id'])) {
            return $this->view(ADMIN_UPDATE, UserModel::load($_POST['id']));
        } elseif (isset($_SESSION['id']) == $_ENV['ROOT_ID']) {
            return $this->view(ADMIN_UPDATE, []);
        } else {
            return $this->view(ADMIN_LOGIN, ['text' => 'Вы не ввели логин или пароль']);
        }
    }

    /**
     * @return void
     */
    public function updating(): void
    {
        if (isset($_SESSION['id']) == $_ENV['ROOT_ID'] && isset($_POST['id'])) {
            UserModel::updates($_POST);
            header('Location: ' . $_ENV['APP_URL'] . ADMIN_UPDATE);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . ADMIN_LOGIN);
        }
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        if (isset($_SESSION['id']) == $_ENV['ROOT_ID'] && isset($_POST['id'])) {
            UserModel::del($_POST['id']);
            header('Location: ' . $_ENV['APP_URL'] . ADMIN_UPDATE);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . ADMIN_LOGIN);
        }
    }

    /**
     * @return void
     */
    public function exit(): void
    {
        unset($_SESSION['id']);
        header('Location: ' . $_ENV['APP_URL'] . ADMIN_LOGIN);
    }
}
