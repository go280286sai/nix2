<?php
/**
 * class ArticleController
 * methods for articles
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Controllers;

use App\Models\ArticleModel;

class ArticleController extends BaseController
{
    /**
     * @return bool
     */
    public function index(): bool
    {
        return $this->view(USER_INDEX, []);
    }

    /**
     * @return bool|null
     */
    public function register(): bool|null
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID']) {
            return $this->view(ARTICLE_REGISTER, []);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
        }
        return true;
    }

    /**
     * @return void
     */
    public function find(): void
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID']) {
            ArticleModel::select($_SESSION['id']);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
        }
    }

    /**
     * @return bool|null
     */
    public function select(): bool|null
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID']) {
            return $this->view(ARTICLE_FIND, []);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function sort($id): bool
    {
        $data = ArticleModel::getSort($id);
        $volume = array_column($data, 'article_id');
        array_multisort($volume, SORT_DESC, $data);
        return $this->view(MAIN_INDEX, $data);
    }

    /**
     * @param $id
     * @return bool
     */
    public function article($id): bool
    {
        $data = ArticleModel::getArticle($id);
        return $this->view(ARTICLE_ARTICLE, [$data]);
    }

    /**
     * @return void
     */
    public function create(): void
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID'] && isset($_POST)) {
            ArticleModel::create($_POST);
            header('Location: ' . $_ENV['APP_URL'] . USER_UPDATE);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
        }
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID'] && isset($_POST)) {
            return $this->view('article/update', ArticleModel::load($_POST));
        } else {
            header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
        }
        return true;
    }

    /**
     * @return void
     */
    public function updating(): void
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID'] && isset($_POST)) {
            ArticleModel::updates($_POST);
            header('Location: ' . $_ENV['APP_URL'] . USER_UPDATE);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
        }
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID'] && isset($_POST)) {
            ArticleModel::del($_POST);
            header('Location: ' . $_ENV['APP_URL'] . USER_UPDATE);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
        }
    }
}
