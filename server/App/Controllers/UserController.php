<?php
/**
 * class UserController
 * methods for articles
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace App\Controllers;

use App\Models\UserModel;
use StorchakProject\framework\src\SessionUsers;

class UserController extends BaseController
{

    /**
     * @return void
     */
    public function index(): void
    {
        header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        return $this->view(USER_LOGIN, ['title' => 'authorization']);
    }

    /**
     * @return bool|void
     */
    public function authorization()
    {
        if (!empty($_POST)) {
            if (UserModel::authorization($_POST)) {
                header('Location: ' . $_ENV['APP_URL'] . USER_UPDATE);
            } else {
                return $this->view(USER_LOGIN, ['text' => 'Вы ввели не верный логин или пароль']);
            }
        } else {
            return $this->view(USER_LOGIN, ['text' => 'Вы не ввели логин или пароль']);
        }
    }

    /**
     * @return bool
     */
    public function register(): bool
    {
        return $this->view(USER_REGISTER, ['title' => 'New registration']);
    }

    /**
     * @return bool
     */
    public function create(): bool
    {
        if (!isset($_SESSION['id']) && isset($_POST)) {
            return $this->view(USER_LOGIN, UserModel::create($_POST));
        } else {
            return $this->view(USER_INDEX, ['title' => 'Authorization']);
        }
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        if (isset($_SESSION['id']) && $_SESSION['id'] != $_ENV['ROOT_ID']) {
            return $this->view(USER_UPDATE, UserModel::load($_SESSION['id']));
        } elseif (isset($_SESSION['id']) == $_ENV['ROOT_ID'] && isset($_POST['id'])) {
            return $this->view(USER_UPDATE, UserModel::load($_POST['id']));
        } else {
            return $this->view(USER_LOGIN, ['text' => 'Вы не ввели логин или пароль']);
        }
    }

    /**
     * @return void
     */
    public function updating(): void
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID'] && isset($_POST)) {
            UserModel::updates($_POST);
            header('Location: ' . $_ENV['APP_URL'] . USER_UPDATE);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . USER_INDEX);
        }
    }

    /**
     * @return void
     */
    public function exit(): void
    {

        $session = new SessionUsers();
        $session->delSession('id');
        unset($_SESSION['id']);
        header('Location: ' . $_ENV['APP_URL'] . USER_LOGIN);
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        if (isset($_SESSION['id']) && ($_SESSION['id']) != $_ENV['ROOT_ID']) {
            UserModel::del($_SESSION['id']);
        }
        header('Location: ' . $_ENV['APP_URL'] . USER_LOGIN);
    }
}
