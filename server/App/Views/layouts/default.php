<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo $_ENV['APP_URL'] ?>/css/style.css" type="text/css" media="all"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="<?php echo $_ENV['APP_URL'] ?>/js/func.js"></script>
    <title><?php echo $title ?? 'Be Smart technology' ?></title>
</head>
<body>
<div class="shell">
    <div class="border">
        <div id="header">
            <h1 id="logo"><a href="<?php echo $_ENV['APP_URL'] ?>" class="notext">beSmart</a></h1>
            <div class="socials right">
                <ul>
                    <li class="last"><a href="<?php echo $_ENV['APP_URL'].USER_UPDATE?>"
                                        class="twit"><?php echo isset($_SESSION['id']) ? 'My account' : 'Autorization' ?></a>
                    </li>
                </ul>
            </div>
            <div class="cl">&nbsp;</div>
        </div>

        <div id="navigation">
            <ul>
                <li><a href="<?php echo $_ENV['APP_URL'] ?>" class="active">Home</a></li>
                <li><a href="<?php echo $_ENV['APP_URL'] ?>/article/sort?id=1">Technology</a></li>
                <li><a href="<?php echo $_ENV['APP_URL'] ?>/article/sort?id=2">Science</a></li>
                <li><a href="<?php echo $_ENV['APP_URL'] ?>/article/sort?id=3">languages</a></li>
                <li><a href="<?php echo $_ENV['APP_URL'] ?>/article/sort?id=4">Other</a></li>
                <li><a href="<?php echo $_ENV['APP_URL'] ?>/main/contact">Contact</a></li>
            </ul>
        </div>
        <div id="main">
            <div class="container">
                <?php
                echo $content??'';
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>


