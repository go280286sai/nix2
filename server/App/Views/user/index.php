<body>
<div class="container tables">
    <h1>Find users</h1>
    <form action="<?php echo $_ENV['APP_URL'].USER_FIND?>" method="post" class="border font-monospace" id="frm_1">
        <div class="mb-3">
            <label class="form-label">First name</label>
            <input type="text" class="form-control" name="where">
        </div>
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
        <button type="button" class="btn btn-primary" name="main" value="main" id="btn_5"
                onclick="window.open('<?php echo $_ENV['APP_URL'].USER_REGISTER?>', '_self')">Add user
        </button>
    </form>
    <div class="success"></div>
</div>
<script>
    $('form').on('submit', function (e) {
        e.preventDefault()
        $.ajax({
            url: $('form').attr('action'),
            method: $('form').attr('method'),
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                let len = data.length;
                let table_td = `<tr>
                        <th>number</th><th>gender</th><th>email</th><th>created_at</th><th>updated_at</th><th>update</th><th>delete</th>
                    </tr>`;
                for (let i = 0; i < len; i++) {
                    table_td += `<tr>`;
                    table_td += `<td>${data[i]['number']}</td>`;
                    table_td += `<td>${data[i]['gender']}</td>`;
                    table_td += `<td>${data[i]['email']}</td>`;
                    table_td += `<td>${data[i]['created_at']}</td>`;
                    table_td += `<td>${data[i]['updated_at']}</td>`;
                    table_td += `<td><form action="<?php echo $_ENV['APP_URL'].USER_UPDATE?>" method="post"><input type="text" hidden name="id" value="${data[i]['number']}"><input type="submit" value="Update"></form></td>`;
                    table_td += `<td><form action="<?php echo $_ENV['APP_URL'].USER_DELETE?>" method="post"><input type="text" hidden name="id" value="${data[i]['number']}"><input type="submit" value="Delete"></form></td>`;
                    table_td += `</tr>`;
                }
                $(".success").html(`<table class="table table-success table-striped">${table_td}</table>`);
            }
        });
    })


</script>
