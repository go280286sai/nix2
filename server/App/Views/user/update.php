<?php
$valid_types = array('jpg', 'png', 'jpeg');
if (isset($_FILES['user_photo'])) {
    $type = explode('/', $_FILES['user_photo']['type']);
    $size = $_FILES['user_photo']['size'];
    if (is_uploaded_file($_FILES['user_photo']['tmp_name'])) {
        if ($_FILES['user_photo']['size'] <= 300000 && in_array($type[1], $valid_types)) {
            $filename = $_FILES['user_photo']['name'];
            $upl_add_dir = ROOT . '/public/images/users/';
            $upl_add_file = $upl_add_dir . 'user_foto_' . $filename;
            $image = $_ENV['APP_URL']. '/images/users/' . 'user_foto_' . $filename;
            move_uploaded_file($_FILES['user_photo']['tmp_name'], $upl_add_file);
        } else {
            $text = 'This file cannot be loaded';
        }
    }
}
?>
<body>
<div class="container tables">
    <div class="row g-0 text-left">
        <div class="col-sm-6 col-md-7"><h1>Welcome, <?php echo $first_name ?? ''; ?>!</h1>
            <form action='<?php echo $_ENV['APP_URL'].USER_UPDATING?>' method="post" class="border font-monospace">
                <div class="mb-3">
                    <label class="form-label">Name</label>
                    <input type="text" class="form-control" name="first_name" value="<?php echo $first_name ?? ''; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Last name</label>
                    <input type="text" class="form-control" name="last_name" value="<?php echo $last_name ?? ''; ?>">
                </div>
                <label class="form-label">Gender</label>
                <select class="form-select" aria-label="Default select example" name="gender">
                    <option selected value=""><?php echo $name ?? ''; ?></option>
                    <option value="0">Not known</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                    <option value="9">Not applicable</option>
                </select>
                <div class="mb-3">
                    <label class="form-label">email</label>
                    <?php echo $err ?? ''; ?>
                    <input type="email" class="form-control" name="email" value="<?php echo $email ?? ''; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">new password</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" hidden="hidden" name="email_old"
                           value="<?php echo $email ?? ''; ?>">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" hidden="hidden" name="img"
                           value="<?php echo $image ?? $_ENV['APP_URL'].'/images/users/user_foto_default.png'; ?>">
                </div>
                <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Update</button>
                <a href="exit">Logout</a>
            </form>
        </div>
        <div class="col-md-5 text-center">
            <div class="col">
                <div class="p-3 border bg-light"><?php
                if (!isset($img)) {
                    echo '<img width="300px" src="' . $_ENV['APP_URL'].'/images/users/user_foto_default.png' . '"</img>';
                } else {
                    echo '<img width="300px" src="' . $img . '"</img>';
                }
                ?></div>
            </div>
            <div class="col">
                <div class="p-3 border bg-light">
                    <form action="<?php echo $_ENV['APP_URL'].USER_UPDATE?>" enctype="multipart/form-data" method="post">
                        <input name="user_photo" type="file">
                        <input type="submit" value="Add File">
                    </form>
                    <?php echo $text ?? 'File not more 250kb and only *.png, *.jpg' ?>
                </div>
            </div>
        </div>
    </div>
    <div align="right"><a href="<?php echo $_ENV['APP_URL'].USER_DELETE?>">Delete account</a></div>
    <form action='<?php echo $_ENV['APP_URL'].ARTICLE_REGISTER?>' method="post" class="border font-monospace" id="frm_1">
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Create</button>
    </form>
    <div class="success"></div>
    <script>
        $(document).ready(function () {
            $.ajax({
                url: '<?php echo $_ENV['APP_URL'].ARTICLE_FIND?>',
                method: 'POST',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    let len = data.length;
                    let table_td = `<tr>
                        <th>id</th><th>title</th><th>description</th><th>created_at</th><th>updated_at</th><th>update</th><th>delete</th>
                    </tr>`;
                    for (let i = 0; i < len; i++) {
                        table_td += `<tr>`;
                        table_td += `<td>${data[i]['article_id']}</td>`;
                        table_td += `<td>${data[i]['title']}</td>`;
                        table_td += `<td>${data[i]['short']}</td>`;
                        table_td += `<td>${data[i]['created_at']}</td>`;
                        table_td += `<td>${data[i]['updated_at']}</td>`;
                        table_td += `<td><form action="<?php echo $_ENV['APP_URL'].USER_UPDATE?>" method="post"><input type="text"  hidden name="id" value="${data[i]['article_id']}"><input type="submit" class="btn btn-primary" value="Update"></form></td>`;
                        table_td += `<td><form action="<?php echo $_ENV['APP_URL'].USER_DELETE?>" method="post"><input type="text" hidden name="id" value="${data[i]['article_id']}"><input type="submit" class="btn btn-primary" value="Delete"></form></td>`;
                        table_td += `</tr>`;
                    }
                    $(".success").html(`<table class="table table-success table-striped">${table_td}</table>`);
                }
            });
        })
    </script>
</div>
</body>

