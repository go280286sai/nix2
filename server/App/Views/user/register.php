<body>
<div class="container tables">
    <h1>New Registration</h1>
    <form action='<?php echo $_ENV['APP_URL'].USER_CREATE?>' method="post" class="border font-monospace">
        <div class="mb-3">
            <label class="form-label">Name</label>
            <input type="text" class="form-control" name="first_name" value="" required="required">
        </div>
        <div class="mb-3">
            <label class="form-label">Last name</label>
            <input type="text" class="form-control" name="last_name" value="" required="required">
        </div>
        <select class="form-select" aria-label="Default select example" name="gender">
            <option selected>Select gender status</option>
            <option value="0">Not known</option>
            <option value="1">Male</option>
            <option value="2">Female</option>
            <option value="9">Not applicable</option>
        </select>
        <div class="mb-3">
            <label class="form-label">email</label>
            <input type="email" class="form-control" name="email" value="" required="required">
        </div>
        <div class="mb-3">
            <label class="form-label">password</label>
            <input type="password" class="form-control" name="password" required="required">
        </div>
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
        <button type="button" class="btn btn-primary" name="main" value="main" id="btn_5" onclick="window.open('<?php echo $_ENV['APP_URL'].USER_LOGIN?>', '_self')">Authorization</button>
    </form>
</body>

