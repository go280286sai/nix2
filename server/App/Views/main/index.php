<?php
echo '<div class="container" style="padding: 20px">';

foreach ($data ?? [] as $item) {
    echo '<a href="' . $_ENV['APP_URL'] . ARTICLE_ARTICLE . '?id='
        . $item['article_id'] . '"><h2>' . html_entity_decode($item['title']) . '</h2></a>';
    echo '<br>';
    echo html_entity_decode($item['short']) . '<br>';
    echo '<a href="' . $_ENV['APP_URL'] . ARTICLE_ARTICLE . '?id=' . $item['article_id'] . '">Read more</a>';
    echo '<br>';
    echo '<br>';
    echo '<br>';
}
echo '</div>';
