<body>
<br/>
<table border="0px">
    <tbody>
    <tr>
        <td width="30%">
            <div class="separator" style="clear: both; text-align: center;">
                <div class="separator" style="clear: both; text-align: center ;">
                    <a href="<?php echo $_ENV['APP_URL'] ?>/images/users/user_foto_logo.jpg" style="margin-left: 1em; margin-right: 1em; ">
                        <img alt="Alexander Storchak" src="<?php echo $_ENV['APP_URL'] ?>/images/users/user_foto_logo.jpg" width="300"/></a></div>
            </div>
        </td>
        <td width="70%">
            <h1>Storchak Aleksander</h1><br/>
            <h3>Residence address: Ukraine, Kharkov</h3>
            <h3>E-mail: <a href="mailto:go280286sai@gmail.com">go280286sai@gmail.com </a></h3>
            <h3>linkedin: <a href="http://www.linkedin.com/in/go280286sai">www.linkedin.com</a></h3>
            <h3>Facebook:&nbsp;<a href=" https://www.facebook.com/go280286sai">https://www.facebook.com</a></h3></td>
    </tr>
    <tr>
        <td width="30%"><h2>Skills</h2>
            <p>- OS Windows, ASP Linux;</p>
            <p>- HTML, CSS, JAVASCRIPT, PHP</p>
            <p>- Joomla, Wordpress, Opencart</p>
            <p>- Python (parsing)</p>
            <p>- Tablea</p>
            <p>- 1C programming (junior)</p>
            <p>- Photoshop, Illustrator; </p>
            <p>- Premiere,After Effects;</p>
        </td>
        <td width="70%"><h2>Education</h2>
            <p>2005-2007 <b>Donetsk Computer Academy "Step"</b>, Faculty - computer graphics, specialist in computer
                graphics and Internet technologies.<br/>Studied 3DMax, all Adobe (Photoshop, dreamweaver, illustrator,
                indesign, etc.), PHP, javascript, CSS, HTML and more. Developed a personal CMS for the real estate
                agency "Continental".</p>
            <p>2003-2008 <b>Automobile and Road Institute of Donetsk National Technical University</b>, Faculty -
                Management of organizations, full-time department, specialty - Manager-economist, diploma of a
                specialist</p>
            <h2>Certificates:</h2>
            <p>2021 Stepic: <a href="https://stepik.org/cert/1071750">Introduction to Linux</a></p>
            <p>2021 Stepic: <a href="https://stepik.org/cert/1067709">Python Programming</a></p>
            <p>2020 Stepic: <a href="https://stepik.org/cert/785674">Python Generation Beginner Course</a></p>
            <p>2021 Stepic: <a href="https://stepik.org/cert/931643">Information technology. Working with Excel
                    spreadsheets</a></p>
            <p>2021 Stepic: <a href="https://stepik.org/cert/928956">Jira: whiteboard issue management</a></p>
            <p>2019 Stepic: <a href="https://stepik.org/cert/241009">JavaScript for beginners</a></p>
            <p>2019 Stepic: <a href="https://stepik.org/cert/233462">Web Development for Beginners: HTML and CSS</a></p>
        </td>
    </tr>
    </tbody>
</table>
</body>

