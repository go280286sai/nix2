<?php
echo '<div class="container" style="padding: 20px">';
foreach ($data ?? [] as $datum) {
    foreach ($datum as $item) {
        echo '<a href="' . $_ENV['APP_URL'] . ARTICLE_ARTICLE . '?id='
            . $item['article_id'] . '"><h2>' . html_entity_decode($item['title']) . '</h2></a>';
        echo '<br>';
        echo html_entity_decode($item['description']);
        echo '<br>';
        echo '<br>';
        echo '<br>';
        echo '<strong>Article created:</strong> ' . $item['created_at'] . '<br>';
        echo '<strong>Author:</strong> ' . html_entity_decode($item['last_name'])
            . ' ' . html_entity_decode($item['first_name']) . '<br>';
        echo '<br>';
        echo '<br>';
    }
}
echo '</div>';
