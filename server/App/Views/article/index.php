<body>
<div class="container tables">
    <h1>Find article</h1>
    <form action="<?php echo ARTICLE_FIND?>" method="post" class="border font-monospace" id="frm_1">
        <div class="mb-3">
            <label class="form-label">First name</label>
            <input type="text" class="form-control" name="where">
        </div>
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
    </form>
    <div class="success"></div>
    <script>
        $('form').on('submit', function (e) {
            e.preventDefault()
            $.ajax({
                url: $('form').attr('action'),
                method: $('form').attr('method'),
                dataType: 'json',
                data: $(this).serialize(),
                success:  function (data) {
                    let len = data.length;
                    let table_td = `<tr>
                        <th>id</th><th>title</th><th>description</th><th>short</th><th>created_at</th><th>updated_at</th><th>update</th><th>delete</th>
                    </tr>`;
                    for (let i = 0; i < len; i++) {
                        table_td+= `<tr>`;
                        table_td += `<td>${data[i]['id']}</td>`;
                        table_td += `<td>${data[i]['title']}</td>`;
                        table_td += `<td>${data[i]['description']}</td>`;
                        table_td += `<td>${data[i]['short']}</td>`;
                        table_td += `<td>${data[i]['created_at']}</td>`;
                        table_td += `<td>${data[i]['updated_at']}</td>`;
                        table_td += `<td><form action="/article/update" method="post"><input type="text" hidden name="id" value="${data[i]['id']}"><input type="submit" value="Update"></form></td>`;
                        table_td += `<td><form action="/article/delete" method="post"><input type="text" hidden name="id" value="${data[i]['id']}"><input type="submit" value="Delete"></form></td>`;
                        table_td+= `</tr>`;
                    }
                    $(".success").html(`<table class="table table-success table-striped">${table_td}</table>`);
                }
            });
        })
    </script>
</body>
