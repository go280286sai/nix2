<body>
<div class="container tables">
    <h1>Edit article</h1>
    <form action="<?php echo ARTICLE_UPDATING?>" method="post" class="border font-monospace" id="frm_1">

        <div class="mb-3">
            <label class="form-label">Title</label>
            <input type="text" class="form-control" name="title" value="<?php echo $title?? '';?>">
        </div>
        <label class="form-label">Sort</label>
        <select class="form-select" aria-label="Default select example" name="sort">
            <option selected value=""><?php echo $name?? '';?></option>
            <option value="1">Technology</option>
            <option value="2">Science</option>
            <option value="3">languages</option>
            <option value="4">Other</option>
        </select>
        <div class="mb-3">
            <label for="floatingTextarea">Short description</label>
            <textarea class="form-control" rows="3" name="short"><?php echo $short?? '';?></textarea>
        </div>
        <div class="mb-3">
            <label for="floatingTextarea">Description</label>
            <textarea class="form-control" rows="13" cols="100%"
                      name="description"><?php echo $description?? '';?></textarea>
        </div>
        <div class="mb-3">
            <input type="text" hidden="hidden" class="form-control" name="id" value="<?php echo $_POST['id']?>">
        </div>
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
        <button type="button" class="btn btn-primary" name="main" value="main" id="btn_5"
                onclick="window.open('/user/update', '_self')">Main
        </button>
    </form>
</div>
</body>
