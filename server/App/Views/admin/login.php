<body>
<div class="container tables">
    <h1>Authorization</h1>
    <form action="<?php echo ADMIN_AUTH ?>" method="post" class="border font-monospace">
        <div class="mb-3">
            <label class="form-label">Input email</label>
            <input type="email" class="form-control" required="required" name="email" value="">
        </div>
        <div class="mb-3">
            <label class="form-label">Input password</label>
            <input type="password" class="form-control" required="required" name="password">
        </div>
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
        <button type="button" class="btn btn-primary" name="main" value="main" id="btn_5"
                onclick="window.open(<?php echo ADMIN_UPDATE ?>, '_self')">Main
        </button>
    </form>
    <?php echo $text ?? ''; ?>
</div>
</body>
