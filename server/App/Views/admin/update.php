<body>
<div class="container tables">
    <div class="row g-0 text-left">
        <div class="col-sm-6 col-md-7"><h1>Welcome, <?php echo $first_name ?? 'root'; ?>!</h1>
            <form action="<?php echo ADMIN_UPDATING ?>" method="post" class="border font-monospace">
                <div class="mb-3">
                    <label class="form-label">Name</label>
                    <input type="text" class="form-control" name="first_name" value="<?php echo $first_name ?? ''; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">Last name</label>
                    <input type="text" class="form-control" name="last_name" value="<?php echo $last_name ?? ''; ?>">
                </div>
                <label class="form-label">Gender</label>
                <select class="form-select" aria-label="Default select example" name="gender">
                    <option selected value=""><?php echo $name ?? ''; ?></option>
                    <option value="0">Not known</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                    <option value="9">Not applicable</option>
                </select>
                <div class="mb-3">
                    <label class="form-label">email</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $email ?? ''; ?>">
                </div>
                <div class="mb-3">
                    <label class="form-label">new password</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" hidden="hidden" name="email_old"
                           value="<?php echo $email ?? ''; ?>">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" hidden="hidden" name="img"
                           value="<?php echo $image ?? $_ENV['APP_URL'].'/images/users/user_foto_default.png'; ?>">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" hidden="hidden" name="id"
                           value="<?php echo $id ?? ''; ?>">
                </div>
                <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
                <a href="exit">Logout</a>
            </form>
        </div>
        <div class="col-md-5 text-center">
            <div class="col">
                <div class="p-3 border bg-light"><?php
                if (!isset($img) || empty($img)) {
                    echo '<img width="300px" src="' . $_ENV['APP_URL'].'/images/users/user_foto_default.png' . '"</img>';
                } else {
                    echo '<img width="300px" src="' . $img . '"</img>';
                }
                ?></div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="container tables">
    <h1>Find users</h1>
    <form action="<?php echo $_ENV['APP_URL'].ADMIN_FIND ?>" method="post" class="border font-monospace">
        <div class="mb-3">
            <label class="form-label">Search</label>
            <label>
                <input type="text" class="form-control" name="search">
            </label>
        </div>
        <button type="submit" class="btn btn-primary" name="submit" value="submit" id="btn_5">Submit</button>
    </form>
    <div class="success"></div>
</div>
<script>$('form').eq(1).on('submit', function (e) {
        e.preventDefault()
        $.ajax({
            url: "<?php echo ADMIN_FIND?>",
            method: $('form').attr('method'),
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                let len = data.length;
                let table_td = `<tr>
                        <th>id</th><th>first_name</th><th>last_name</th><th>name</th><th>email</th><th>created_at</th><th>updated_at</th><th>Update</th><th>Delete</th>
                    </tr>`;
                for (let i = 0; i < len; i++) {
                    table_td += `<tr>`;
                    table_td += `<td>${data[i]['id']}</td>`;
                    table_td += `<td>${data[i]['first_name']}</td>`;
                    table_td += `<td>${data[i]['last_name']}</td>`;
                    table_td += `<td>${data[i]['name']}</td>`;
                    table_td += `<td>${data[i]['email']}</td>`;
                    table_td += `<td>${data[i]['created_at']}</td>`;
                    table_td += `<td>${data[i]['updated_at']}</td>`;
                    table_td += `<td><form action="<?php echo ADMIN_UPDATE ?>" method="post"><input type="text"  hidden name="id" value="${data[i]['id']}"><input type="submit" class="btn btn-primary" value="Update"></form></td>`;
                    table_td += `<td><form action="<?php echo ADMIN_DELETE ?>" method="post"><input type="text" hidden name="id" value="${data[i]['id']}"><input type="submit" class="btn btn-primary" value="Delete"></form></td>`;
                    table_td += `</tr>`;
                }
                $(".success").html(`<table class="table table-success table-striped">${table_td}</table>`);
            }
        });
    })</script>
</body>
