<?php
/**
 * list routes
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
use StorchakProject\framework\src\Route;

Route::add('^/?$');
Route::add('^/(?P<controller>[a-z-]+)/?$');
Route::add('^/(?P<controller>[a-z-]+)/(?P<action>[a-z-]+)/?$');
Route::add(('^/(?P<controller>[a-z-]+)/(?P<action>[a-z-]+)[?](?P<params>[a-z-=0-9]+)/?$'));
