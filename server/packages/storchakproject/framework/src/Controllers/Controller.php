<?php
/**
 * abstract class Controller
 * return views
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\framework\src\Controllers;

use StorchakProject\framework\src\Views\View;

abstract class Controller
{
    /**
     * @var array
     */
    public array $route = [];
    /**
     * @var array
     */
    public array $data = [];

    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * @param string $view
     * @param array $data
     * @return bool
     */
    public function view(string $view, array $data = []): bool
    {
        if (!empty($this->data)) {
            $data = array_merge($this->data, $data);
        }
        $viewObject = new View($this->route, $view);
        $viewObject->render($data);
        return true;
    }
}
