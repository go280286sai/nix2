<?php
/**
 * class SessionUsers
 * save session data to db
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace StorchakProject\framework\src;

class SessionUsers
{
    protected object $pdo;
    protected string $table = 'users_id';

    public function __construct()
    {
        $this->pdo = DB::instance();
    }

    /**
     * @param string $name
     * @param int $id
     * @return bool
     */
    public function setSession(string $name, int $id): bool
    {
        $date = date('Y-m-d');
        $ip_users = $_SERVER['REMOTE_ADDR'];
        $sql = "INSERT INTO $this->table (name, id, date, ip_users) VALUES ('$name', $id, '$date', '$ip_users')";
        return $this->pdo->query($sql);
    }


    public function getSession(string $name): bool
    {
        $ip_users = $_SERVER['REMOTE_ADDR'];
        $sql = "SELECT * FROM $this->table WHERE name='$name' and ip_users='$ip_users'";
        if (empty($this->pdo->query($sql))) {
            return false;
        } else {
            $result = $this->pdo->query($sql)[0];
            $_SESSION['id'] = $result['id'];
            return $_SESSION['id'];
        }
    }


    /**
     * @param string $name
     * @return void
     */
    public function delSession(string $name): void
    {
        $ip_users = $_SERVER['REMOTE_ADDR'];
        $sql = "DELETE FROM $this->table WHERE name='$name' and ip_users='$ip_users'";
        $this->pdo->query($sql);
    }
}
