<?php
/**
 * class View
 * output content
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\framework\src\Views;

class View
{
    /**
     * @var array
     */
    public array $route = [];
    /**
     * @var string
     */
    public string $view;

    public function __construct(array $route, string $view = '')
    {
        $this->view = $view;
        $this->route = $route;
    }

    /**
     * @param $data
     * @return string
     */
    public function render($data): string
    {
        if (is_array($data)) {
            extract($data);
        }
        ob_start();
        $fileView = ROOT . '/App/Views/' . $this->view . '.php';
        if (is_file($fileView)) {
            require $fileView;
        } else {
            echo '<h1>File not found' . $fileView . '</h1>';
        }
        $content = ob_get_clean();
        $fileLayouts = ROOT . '/App/Views/layouts/' . 'default' . '.php';
        if (is_file($fileLayouts)) {
            require $fileLayouts;
        } else {
            echo 'File not fount ' . $fileLayouts;
        }
        return $content;
    }
}
