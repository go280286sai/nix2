<?php

/**
 * class Singleton
 * to call Route
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace StorchakProject\framework\src;

class Singleton
{
    /**
     * @var object|null
     */
    protected static ?object $instance = null;

    /**
     *
     */
    protected function __construct()
    {
    }

    /**
     * @return void
     */
    protected function __clone(): void
    {
    }

    /**
     * @return DB
     */
    public static function instance(): DB
    {
        if (self::$instance === null) {
            self::$instance = new DB;
        }
        return self::$instance;
    }
}
