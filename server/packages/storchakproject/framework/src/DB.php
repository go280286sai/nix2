<?php
/**
 * class DB
 * connect and database query
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\framework\src;

use Exception;
use PDO;
use StorchakProject\Logger\src\Logger;

class DB extends Singleton
{
    /**
     * @var PDO $pdo
     */
    protected PDO $pdo;

    protected function __construct()
    {
        parent::__construct();
        $db = require ROOT . '/config/database.php';
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];
        $this->pdo = new PDO($db['dns'], $db['username'], $db['password'], $options);
    }

    /**
     * @param $sql
     * @param array $param
     * @return array
     */
    public function query($sql, array $param = []): array
    {
        $PDOStatement = $this->pdo->prepare($sql);
        try {
            $result = $PDOStatement->execute($param);
            if ($result !== false) {
                return $PDOStatement->fetchAll();
            }
        } catch (Exception $e) {
//            echo $e->getMessage();
            Logger::log($e->getMessage());
        }
        echo 'ПО данному запросу ничего не нашли';
        return [];
    }
}
