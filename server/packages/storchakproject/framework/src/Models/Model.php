<?php
/**
 * class Model
 * database methods
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\framework\src\Models;

use StorchakProject\framework\src\DB;

abstract class Model
{
    /**
     * @var DB
     */
    protected DB $pdo;
    /**
     * @var string
     */
    protected string $table;
    /**
     * @var string
     */
    protected string $table_join;
    /**
     * @var bool
     */
    protected bool $created_at = false;
    /**
     * @var bool
     */
    protected bool $updated_at = false;
    /**
     * @var bool
     */
    protected bool $closeSelection = false;

    public function __construct()
    {
        $this->pdo = DB::instance();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $sql = "SELECT * FROM $this->table_join";
        $result = $this->pdo->query($sql);
        $arr = [];
        foreach ($result as $item) {
            $lock = [$item['password']];
            $mas = array_filter($item, fn($elem) => (!in_array($elem, $lock)));
            $arr[] = $mas;
        }
        return $arr;
    }

    /**
     * @param array $param
     * @param string $search
     * @param string $operator
     * @param string $id
     * @return array
     */
    public function where(
        array  $param,
        string $search,
        string $operator,
        string $id
    ): array {
        $strParam = implode(',', $param);
        $sql = "SELECT $strParam FROM $this->table_join WHERE $search $operator $id";
        return $this->pdo->query($sql);
    }


    /**
     * @param array $param
     * @param string $search
     * @param string $operator
     * @param string $id
     * @return array
     */
    public function whereOne(
        array  $param,
        string $search,
        string $operator,
        string $id
    ): array {
        $strParam = implode(',', $param);
        $sql = "SELECT $strParam FROM $this->table_join  WHERE $search $operator '$id' limit 1";
        if (!empty($this->pdo->query($sql))) {
            return $this->pdo->query($sql)[0];
        } else {
            return [];
        }
    }

    /**
     * @param $array
     * @return array
     */
    public function insert($array): array
    {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);

        if ($this->created_at && $this->updated_at) {
            $arr = [
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ];
            $array = array_merge($array, $arr);
        }
        $keysString = implode(',', array_keys($array));
        $valuesString = '';
        foreach ($array as $v) {
            $valuesString .= '\'' . $v . '\'' . ',';
        }
        $valuesString = rtrim($valuesString, ',');
        $sql = "INSERT INTO $this->table ($keysString) VALUES ($valuesString)";
        return $this->pdo->query($sql);
    }

    /**
     * @param array $param
     * @param string $column
     * @param string $id
     * @return array
     */
    public function update(
        array  $param,
        string $column,
        string $id
    ): array {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $param);
        $sqlTxt = '';
        foreach ($array as $k => $v) {
            $sqlTxt .= $k . '=' . '\'' . $v . '\'' . ',';
        }
        $sqlTxt = rtrim($sqlTxt, ',');
        $sql = "UPDATE $this->table SET $sqlTxt WHERE $column = '$id'";
        return $this->pdo->query($sql);
    }

    /**
     * @param string $search
     * @param string $operator
     * @param string $id
     * @return array
     */
    public function delete(
        string $search,
        string $operator,
        string $id
    ): array {
        $sql = "DELETE FROM $this->table WHERE $search $operator $id";
        return $this->pdo->query($sql);
    }
}
