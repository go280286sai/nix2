<?php
/**
 * class FileClassFactory
 * save logs by write in file
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\Logger\src\Methods;

use StorchakProject\Logger\src\Singleton;

class FileClassFactory implements MethodInterfaceFactory
{
    use Singleton;

    /**
     * @param $level
     * @param $message
     * @return void
     */
    public function writeLog($level, $message): void
    {
        $file = '/storage/logs/' . date('Y-m-d') . '.txt';
        $handler = fopen($_SERVER['DOCUMENT_ROOT'] . $file, 'a+');
        $date = date('Y-n-d G:i:s');
        $str = $level . '|' . $date . ' ' . print_r($message, true) . "\r\n";
        fwrite($handler, $str);
    }
}
