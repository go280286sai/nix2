<?php
/**
 * interface MethodInterfaceFactory
 * implement function write log
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
namespace StorchakProject\Logger\src\Methods;

interface MethodInterfaceFactory
{
    public function writeLog($level, $message);
}
