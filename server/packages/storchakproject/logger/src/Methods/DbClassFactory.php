<?php

/**
 * class DbClassFactory
 * sending logs by write in db
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\Logger\src\Methods;

use PDO;
use StorchakProject\Logger\src\Singleton;

class DbClassFactory implements MethodInterfaceFactory
{
    use Singleton;
    /**
     * @var PDO
     */
    protected PDO $connect;

    protected function __construct()
    {
        $db = require ROOT . '/config/database.php';
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];
        $this->connect = new PDO($db['dns'], $db['username'], $db['password'], $options);
    }

    /**
     * @param $level
     * @param $message
     * @return void
     */
    public function writeLog($level, $message): void
    {
        $message = htmlspecialchars($message);
        $level = __FUNCTION__;
        $code_http = http_response_code();
        $date = date('Y-m-d');
        $log = self::getInstance();
        $log->connect->query("insert into logs (date, level, message, code_http) 
values ('$date', '$level', '$message', $code_http)");
    }
}
