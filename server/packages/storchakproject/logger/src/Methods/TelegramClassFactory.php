<?php

/**
 * class TelegramClassFactory
 * sending logs by telegram
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\Logger\src\Methods;

use StorchakProject\Logger\src\Singleton;

class TelegramClassFactory implements MethodInterfaceFactory
{
    use Singleton;

    /**
     * @var string
     */
    private string $apiToken = "5495357114:AAF0MDVaN9Uv7Ip3xI0zV53s6XTPcgG8neg";

    /**
     * @param $level
     * @param $message
     * @return void
     */
    public function writeLog($level, $message): void
    {
        $date = date('Y-n-d G:i:s');
        $str = $level . '|' . $date . ' ' . print_r($message, true);
        $data = [
            'chat_id' => '-778339786',
            'text' => $str];
        file_get_contents('https://api.telegram.org/bot' . $this->apiToken . '/sendMessage?' .
            http_build_query($data));
    }
}
