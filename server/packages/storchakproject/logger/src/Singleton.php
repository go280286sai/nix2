<?php
/**
 * class Singleton
 * to implement functionality Logger
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\Logger\src;

use StorchakProject\Logger\src\Methods\DbClassFactory;
use StorchakProject\Logger\src\Methods\FileClassFactory;
use StorchakProject\Logger\src\Methods\TelegramClassFactory;

trait Singleton
{
    /**
     * @var object|null
     */
    private static ?object  $instances = null;

    protected function __construct()
    {
    }

    /**
     * @return Singleton|DbClassFactory|FileClassFactory|TelegramClassFactory
     */
    public static function getInstance(): self
    {
        if (!isset(self::$instances)) {
            self::$instances = new self;
        }
        return self::$instances;
    }

    protected function __clone(): void
    {
        // TODO: Implement __clone() method.
    }
}
