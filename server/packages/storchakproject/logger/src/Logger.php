<?php
/**
 * class Logger
 * writes log by the selected method
 * @author Alexander Storchak <go280286sai@gmail.com>
 */

namespace StorchakProject\Logger\src;

class Logger
{
    /**
     * @param string $context
     * @return void
     */
    public static function log(string $context): void
    {
        self::method($_ENV['PATH_LOGGER'])->writeLog(__FUNCTION__, $context);
    }

    /**
     * @param string $context
     * @return void
     */
    public static function error(string $context): void
    {
        self::method($_ENV['PATH_LOGGER'])->writeLog(__FUNCTION__, $context);
    }

    /**
     * @param string $class
     * @return mixed|void
     */
    public static function method(string $class)
    {
        $listenerClass = 'StorchakProject\\Logger\\src\\Methods\\' . ucfirst($class) . 'ClassFactory';
        if (class_exists($listenerClass)) {
            return $listenerClass::getInstance();
        } else {
            echo $listenerClass;
            exit('This class does not exits' . ucfirst($class) . 'ClassFactory');
        }
    }
}
