<?php
require_once "../vendor/autoload.php";
require_once dirname(__DIR__) . '/config/params.php';
use App\App;
use StorchakProject\framework\src\SessionHandler;
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load(ROOT . '/.env');
$sessionHandler = SessionHandler::getInstance();
session_set_save_handler($sessionHandler, true);
session_start();

new App();
