$('form').eq(1).on('submit', function (e) {
    e.preventDefault()
    $.ajax({
        url: "<?php echo ADMIN_FIND?>",
        method: $('form').attr('method'),
        dataType: 'json',
        data: $(this).serialize(),
        success: function (data) {
            let len = data.length;
            let table_td = `<tr>
                        <th>id</th><th>first_name</th><th>last_name</th><th>name</th><th>email</th><th>created_at</th><th>updated_at</th><th>Update</th><th>Delete</th>
                    </tr>`;
            for (let i = 0; i < len; i++) {
                table_td += `<tr>`;
                table_td += `<td>${data[i]['id']}</td>`;
                table_td += `<td>${data[i]['first_name']}</td>`;
                table_td += `<td>${data[i]['last_name']}</td>`;
                table_td += `<td>${data[i]['name']}</td>`;
                table_td += `<td>${data[i]['email']}</td>`;
                table_td += `<td>${data[i]['created_at']}</td>`;
                table_td += `<td>${data[i]['updated_at']}</td>`;
                table_td += `<td><form action="/admin/update" method="post"><input type="text"  hidden name="id" value="${data[i]['id']}"><input type="submit" class="btn btn-primary" value="Update"></form></td>`;
                table_td += `<td><form action="/admin/delete" method="post"><input type="text" hidden name="id" value="${data[i]['id']}"><input type="submit" class="btn btn-primary" value="Delete"></form></td>`;
                table_td += `</tr>`;
            }
            $(".success").html(`<table class="table table-success table-striped">${table_td}</table>`);
        }
    });
})